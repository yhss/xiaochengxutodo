const AV = require('../../utils/av-weapp-min');

var user = AV.User.current();
var query = new AV.Query('Note');
query.equalTo('user', user.id).find().then(function(results){
    thisnote =  results;
});

Page({
  data: {
    user: null,
    note: '',
    noteid: '',
    error: null,
  },
  onLoad: function() {
    var query = new AV.Query('Note');
    query.equalTo('user', user.id).find().then(this.getNote);
  },
  getNote: function (note) {
      console.log(note.length);
    if(note.length ==0){
        //   // 新建
        var TodoFolder = AV.Object.extend('Note');
        var todoFolder = new TodoFolder();
        todoFolder.set('user',user.id);
        todoFolder.save().then(function (newnote) {
            console.log('新建note is ' + newnote.id);
        }, function (error) {
            console.error(error);
        });
    }else{
        this.setData({
            note:note[0].attributes.content,
            noteid:note[0].id,
        });
    }
  },
  bindTextAreaBlur: function(e) {
    console.log(this.data.noteid)
    console.log(e.detail.value)
        //保存
        var todo = AV.Object.createWithoutData('Note', this.data.noteid);
        todo.set('content',e.detail.value);
        todo.save();

//   // 新建对象
//   var TodoFolder = AV.Object.extend('Note');
//   var todoFolder = new TodoFolder();
//   todoFolder.set('content',e.detail.value);
//   todoFolder.set('user',user.id);
//   todoFolder.save().then(function (todo) {
//     console.log('objectId is ' + todo.id);
//   }, function (error) {
//     console.error(error);
//   });
  }
})